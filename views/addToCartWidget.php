<?php
    use yii\helpers\Url;

    if($allowToAddCart)
    {
        //jika menggunakan function
        if($refreshWidgetFunction!==null)
        {
            $this->registerJs("
                function afterActionRefreshCartWidget()
                {
                    {$refreshWidgetFunction};
                }
            ");
        }
        else {
            $this->registerJs("
                function afterActionRefreshCartWidget()
                {
                    $('#cartWidget').load('{$refreshWidgetUrl}');
                }
            ");   
        }
        //jika pakai modal
        if($modalID)
        { 
            $this->registerJs("
                $(document).on('click', '{$btnAdd}', function(){
                    var that = $(this);
                    var productID = that.data('product-id');
                    //ambil data dari online
                    var content = $('#{$modalID}').find('.modal-body');
                    $('#{$modalID}').modal('show');
                    $.ajax({
                        url: '{$ajaxUrl}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            product_id: productID
                        },
                        success: function(data){
                            content.data('product-id', productID);
                            content.find('.input-qty').val(data.qty);
                            content.find('.label-price').html($.number(data.price, 0, ',', '.'));
                            content.find('.label-name').html(data.product_name);
                            afterActionRefreshCartWidget();
                        }
                    });
                });
            ");
            
            $this->registerJs("
                $('#{$modalID}').on('change', '.input-qty', function(e){
                    var content = $('#{$modalID}').find('.modal-body');
                    var productID = content.data('product-id');
                    var qty = content.find('.input-qty').val();
                    $.ajax({
                        url: '{$ajaxUrlUpdate}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            product_id: productID,
                            qty: qty
                        },
                        success: function(data){
                            content.find('.input-qty').val(data.qty);
                            afterActionRefreshCartWidget();
                            e.preventDefault();
                        }
                    });
                });

                $('#{$modalID}').on('click', '.btn-remove', function(e){
                    var content = $('#{$modalID}').find('.modal-body');
                    var productID = content.data('product-id');
                    $.ajax({
                        url: '{$ajaxUrlDelete}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            product_id: productID
                        },
                        success: function(data){
                            $('#{$modalID}').modal('hide');
                            afterActionRefreshCartWidget();
                        }
                    });
                });
            ");
        }
        //jika tidak
        else {
            $this->registerJs("
                $(document).on('click', '{$btnAdd}', function(){
                    var that = $(this);
                    var productID = that.data('product-id');
                    $.ajax({
                        url: '{$ajaxUrl}',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            product_id: productID
                        },
                        success: function(data){
                            afterActionRefreshCartWidget();
                        }
                    });
                });
            ");
        }
    }
    else {
        $this->registerJs("
        $(document).on('click', '{$btnAdd}', function(){
            document.location = '".Url::to(\Yii::$app->user->loginUrl)."';
        });
        ");
    }
?>
<!-- Modal -->
<div class="modal fade" id="<?= $modalID ?>" tabindex="-1" role="dialog" aria-labelledby="modalAddToCart" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambahkan ke Keranjang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-3">
                  <input type="number" value="1" class="form-control input-qty" min="1" />
              </div>
              <div class="col">
                  <span class="label-name"></span>
                  <br />
                  <label>Rp <span class="label-price"></span></label>
              </div>
              <div class="col-md-2 text-right">
                  <i class="fa fa-trash btn-remove" style="cursor:pointer"></i>
              </div>
          </div>
      </div>
      <div style="padding:15px">
          <div class="row">
              <div class="col-xs-12 col-md-6">
                  <a class="btn btn-secondary btn-block" href="<?= $cartIndexUrl ?>">Lanjut ke pembayaran</a>
              </div>
              <div class="col-xs-12 col-md-6">
                <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Lanjutkan belanja</button>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>