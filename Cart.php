<?php

namespace morningbird\cart;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use morningbird\cart\models\CartModel;

class Cart extends \yii\base\Component {
    
    public $productClass;
    public $cartClass;
    private $_items;

    public function init()
    {
        parent::init();
        CartModel::$_productClass = $this->productClass;
    }
    
    public function getAttributeTotal($key)
    {
        $total = 0;
        foreach($this->items as $item)
        {
            $total += $item->$key;
        }
        return $total;
    }
    
    public function clear()
    {
        if(Yii::$app->user->isGuest)
        {
            CartModel::deleteAll([
                'session_id' => \Yii::$app->session->getId()
            ]);
        }
        else {
            CartModel::deleteAll([
                'users_id' => \Yii::$app->user->id
            ]);
        }
    }

    public function getTotalItems()
    {
        return count($this->getItems());
    }
    
    public function getTotalPrice()
    {
        $total = 0;
        foreach($this->items as $item)
        {
            $total += $item->total;
        }
        return $total;
    }

    public function getTotalWeight()
    {
        $total = 0;
        foreach($this->items as $item)
        {
            $total += $item->weight;
        }
        return $total;
    }

    public function moveSessionCartToCurrentUser($sessionID)
    {
        $tableName = CartModel::tableName();
        $query = Yii::$app->db->createCommand()
                ->update($tableName, [
                    'users_id' => Yii::$app->user->id
                ], ['session_id' => $sessionID]);
        return $query->execute();
    }
    
    public function getItems() {
        if(empty($this->_items))
        {
            if(!\Yii::$app->session->isActive)
            {
                \Yii::$app->session->open();
            }
            if(Yii::$app->user->isGuest)
            {
                $sessionID = \Yii::$app->session->getId();
                $this->_items = CartModel::findAll([
                    'session_id' => $sessionID
                ]);
            }
            else {
                $this->_items = CartModel::findAll([
                    'users_id' => @\Yii::$app->user->id
                ]);
            }
        }
        return $this->_items;
    }
    
    public function deleteItem($product) {
        if(!\Yii::$app->session->isActive)
        {
            \Yii::$app->session->open();
        }
        $sessionID = \Yii::$app->session->getId();
        if(is_object($product))
        {
            if(Yii::$app->user->isGuest)
            {
                $cart = CartModel::findOne([
                    'session_id' => $sessionID,
                    'product_id' => $product->id
                ]);
            }
            else {
                $cart = CartModel::findOne([
                    'users_id' => \Yii::$app->user->id,
                    'product_id' => $product->id
                ]);
            }
        }
        else {
            $cart = CartModel::findOne($product);
        }
        
        if($cart!==null)
        {
            return $cart->delete();
        }
    }
    
    public function updateItems($product, $qty) {
        $sessionID = \Yii::$app->session->getId();
        if(is_object($product))
        {
            $cart = CartModel::findOne([
                'users_id' => \Yii::$app->user->id,
                'product_id' => $product->id
            ]);
            if($cart===null) return null;
            
            if(!\Yii::$app->user->isGuest)
            {
                $cart->users_id = \Yii::$app->user->id;
            }
        }
        else {
            $cart = CartModel::findOne($product);
        }
        if($qty>0)
        {
            $cart->qty = $qty;
            if($cart->save(false))
            {
                return $cart;
            }
            else {
                return null;
            }
        }
        else {
            $cart->delete();
            return null;
        }
    }
    
    public function addItems($product, $qty = 1) {
        if(!\Yii::$app->session->isActive)
        {
            \Yii::$app->session->open();
        }
        $sessionID = \Yii::$app->session->getId();
        $cart = CartModel::findOne([
            'users_id' => \Yii::$app->user->id,
            'product_id' => $product->id
        ]);
        
        if($cart===null)
        {
            $cart = new CartModel();
            $cart->session_id = $sessionID;
            $cart->product_id = $product->id;
            $cart->product_name = $product->name;
            $cart->image_url = $product->image;
            if($product->hasProperty('weight'))
            {
                $cart->weight = $product->weight;
            }
        }
        
        if(!\Yii::$app->user->isGuest)
        {
            $cart->users_id = \Yii::$app->user->id;
        }
        $cart->qty += $qty;
        if($cart->save(false))
        {
            return $cart;
        }
        else {
            return null;
        }
    }
    
    public function getWidget()
    {
        $items = $this->getItems();
        $cartItem = '<div class="dropdown-item"><a>Keranjang Anda kosong</a><div>';
        $cartItemCount = count($items);
        $mobDetect = new \Mobile_Detect();
        if(!$mobDetect->isMobile() && !$mobDetect->isTablet())
        {
            if($cartItemCount>0)
            {
                $cartContent = '';
                foreach($items as $item)
                {
                    $tmpUrl = $item->image_url;
                    $cartContent .= '<a href="'.Url::to(['cart/index']).'" class="cart-wrapper dropdown-item">';
                    $cartContent .= Html::tag('div', Html::img($tmpUrl, ['class' => 'cart-item-img rounded-circle']), ['class' => 'float-left cart-item-img-wrapper']);
                    $description = Html::tag('div', $item->product_name, ['class' => 'cart-item-title']);
                    $description .= Html::tag('div', $item->qty . ' buah', ['class' => 'cart-item-qty']);
                    $cartContent .= Html::tag('div', $description);
                    $cartContent .= Html::tag('div', '', ['class' => 'clearfix']);
                    $cartContent .= '</a>';
                }
                $cartItem = $cartContent;
            }
            $cartItem .= '<span class="divider"></span>';
            $cartItem .= '<a class="dropdown-item text-center" href="'.Url::to(['cart/index']).'">Lihat keranjang</a>';

            $cartLink = ''
                    . '<a href="#" id="btnCart" data-toggle="dropdown" class="nav-link" style="color:white">'
                    . '<span class="fa fa-shopping-cart"></span>'
                    . '<span class="badge badge-warning" style="position:absolute;top: 10px;left: 18px;font-size:10px;padding:3px 5px;border-radius: 50px;">'.$cartItemCount.'</span>'
                    . '</a>'
                    . '<div class="dropdown-menu cart-dropdown" style="min-width:250px">'
                    . $cartItem
                    . '</div>'
                    . '';
        }
        else
        {
            $cartLink = ''
                    . '<a href="'.Url::to(['cart/index']).'" id="btnCart" class="nav-link" style="color:white">'
                    . '<span class="fa fa-shopping-cart"></span>'
                    . '<span class="badge badge-warning" style="position:absolute;top: 10px;left: 18px;font-size:10px;padding:3px 5px;border-radius: 50px;">'.$cartItemCount.'</span>'
                    . '</a>';
        }
        return $cartLink;
    }
}
