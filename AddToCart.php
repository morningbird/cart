<?php
namespace morningbird\cart;


class AddToCart extends \yii\base\Widget {
    public $btnAdd;
    public $btnRemove;
    public $ajaxUrl;
    public $ajaxUrlUpdate;
    public $ajaxUrlDelete;
    public $cartIndexUrl;
    public $refreshWidgetUrl;
    public $refreshWidgetFunction;
    public $modalID;
    public $allowToAddBeforeLogin = false;
    
    public function run() {
        
        $allowToAddCart = true;
        if(!$this->allowToAddBeforeLogin && \Yii::$app->user->isGuest)
        {
            $allowToAddCart = false;
        }
        if($this->refreshWidgetFunction==null)
        {
            $this->modalID = 'modalAddToCart';
        }
        return $this->render('addToCartWidget', [
            'btnAdd' => $this->btnAdd,
            'btnRemove' => $this->btnRemove,
            'ajaxUrl' => $this->ajaxUrl,
            'ajaxUrlUpdate' => $this->ajaxUrlUpdate,
            'ajaxUrlDelete' => $this->ajaxUrlDelete,
            'cartIndexUrl' => $this->cartIndexUrl,
            'allowToAddCart' => $allowToAddCart,
            'refreshWidgetUrl' => $this->refreshWidgetUrl,
            'refreshWidgetFunction' => $this->refreshWidgetFunction,
            'modalID' => $this->modalID
        ]);
    }
}
