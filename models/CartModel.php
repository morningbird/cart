<?php

namespace morningbird\cart\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property string $id
 * @property string $users_id
 * @property string $session_id
 * @property string $product_id
 * @property string $product_name
 * @property string $qty
 * @property string $price
 */
class CartModel extends \yii\db\ActiveRecord
{
    static $_productClass;
    private $_productModel;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }
    
    public function init() {
        parent::init();
        
        $this->qty = 0;
    }

    public function toArray(array $fields = array(), array $expand = array(), $recursive = true) {
        $fields = ['id', 'product_id', 'product_name', 'qty'];
        $arr = parent::toArray($fields, $expand, $recursive);
        $arr['price'] = $this->price;
        return $arr;
    }
    
    public function getTotal()
    {
        return $this->price * $this->qty;
    }

    public function getTotalWeight()
    {
        return $this->weight * $this->qty;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'product_id', 'qty'], 'integer'],
            [['price'], 'number'],
            [['session_id'], 'string', 'max' => 100],
            [['product_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'users_id' => 'Users ID',
            'session_id' => 'Session ID',
            'product_id' => 'Product ID',
            'product_name' => 'Product Name',
            'qty' => 'Qty',
            'price' => 'Price',
        ];
    }

    public function loadProductModel()
    {
        if(empty($this->_productModel))
        {
            $this->_productModel = call_user_func(self::$_productClass .'::find')
                                        ->where(['id' => $this->product_id])
                                        ->one();
        }
    }

    public function getPrice()
    {
        $this->loadProductModel();
        try {
            $this->_productModel->qty = $this->qty;
        } catch (\Exception $exc) {

        }
        return $this->_productModel->price;
    }

    public function getSKU()
    {
        $this->loadProductModel();
        return $this->_productModel->sku;
    }
    
    public function getProductProperty($key)
    {
        $this->loadProductModel();
        if($this->_productModel->hasAttribute($key))
        {
            return $this->_productModel->$key;
        }
        return null;
    }
}
